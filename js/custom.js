import { post } from "axios";
// get data
const getData = async () => {
  // const response = await axios.get('http://localhost/sookh/seo-extension');
  // axios post
  const response = await post("http://localhost/sookh/seo-extension", {
    url: "http://localhost/sookh/seo-extension",
    title: "SEO Extension",
    description: "SEO Extension",
    image: "http://localhost/sookh/seo-extension",
    keywords: "SEO Extension",
  });
  console.log(response.data);

  return response.data;
};
// get data and display in the html
getData().then((data) => {
  document.getElementById("result").innerHTML = data;
});
