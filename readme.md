# Create Browser Extension for Google Chrome

## What is it?

This is a browser extension for Google Chrome. It is a simple extension that allows you to create a new tab and open a new tab in the same window.

## How to use it?

1. Click the button below to create a new tab.
2. Click the button below to open a new tab in the same window.

## How to install it?

1. Click the button below to install the extension.
2. Click the button below to open the extension.

## How to uninstall it?

1. Click the button below to uninstall the extension.
2. Click the button below to close the extension.

## How to update it?

1. Click the button below to update the extension.
2. Click the button below to open the extension.

## How to report a bug?

1. Click the button below to report a bug.
2. Click the button below to open the bug report page.
